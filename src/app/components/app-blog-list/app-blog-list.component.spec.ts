import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppBlogListComponent } from './app-blog-list.component';

describe('AppBlogListComponent', () => {
  let component: AppBlogListComponent;
  let fixture: ComponentFixture<AppBlogListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppBlogListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppBlogListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
