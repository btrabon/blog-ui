import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BlogDetail } from '../../services/blog-detail-list.service';
import { DateService } from '../../services/date.service';

@Component({
  selector: 'app-blog-list',
  templateUrl: './app-blog-list.component.html',
  styleUrls: ['./app-blog-list.component.scss']
})
export class AppBlogListComponent implements OnInit {
  @Input() data: BlogDetail;
  @Input() blogDetailUrl: string;
  @Input() isAdmin: boolean;
  @Input() disabled: boolean;
  @Input() payToView = false;

  @Output() removeClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() payButtonClicked: EventEmitter<any> = new EventEmitter<any>();

  previewTextLength = this.payToView ? 200 : 50;

  get availableInDays() {
    const currentDate = new Date();
    const publishDate = new Date(this.data.date_to_publish);
    const differenceDays = this.dateService.getDifferenceInDays(publishDate, currentDate);
    return differenceDays > 0 ? differenceDays : 0;
  }

  constructor(private dateService: DateService) { }

  ngOnInit() {
  }

  onRemoveClicked = () => {
    this.removeClicked.emit(this.data);
  }

  onPayToViewClicked = () => {
    this.payButtonClicked.emit(this.data);
  }

  showPreviewContent(): string {
    let previewContent = this.data.content;

    // remove any HTML tags before checking the length
    previewContent = this.removeHtmlTags(previewContent);

    if (previewContent.length > this.previewTextLength) {
      previewContent = previewContent.substring(0, this.previewTextLength) + '...';
    }

    if (this.disabled && this.availableInDays > 0) {
      previewContent += ' Available in ' + this.availableInDays.toString() + (this.availableInDays > 1 ? ' days' : ' day');
    }

    return previewContent;
  }

  private removeHtmlTags(content: string): string {
    const finalContent = content.replace('<div>', '')
                              .replace('</div>', '')
                              .replace('<span>', '')
                              .replace('</span>', '')
                              .replace('<br>', '');

    return finalContent;
  }
}
