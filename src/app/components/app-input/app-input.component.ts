import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import { AbstractControl } from '@angular/forms';
import { getErrors } from '../../validators/validationErrorParser';

@Component({
  selector: 'app-input',
  templateUrl: './app-input.component.html',
  styleUrls: ['./app-input.component.scss']
})
export class AppInputComponent implements OnInit {
  @Input() placeholder = '';
  @Input() type = 'text';
  @Input() control?: AbstractControl = null;
  @Input() showParentErrors = false;

  @Output() blur = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  errorMessages() {
    return getErrors(this.showParentErrors, this.control);
  }

  onBlur(event) {
    this.blur.emit(event);
  }
}
