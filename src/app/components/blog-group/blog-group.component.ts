import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BlogGroup } from '../../services/blog-post-group.service';

@Component({
  selector: 'app-blog-group',
  templateUrl: './blog-group.component.html',
  styleUrls: ['./blog-group.component.scss']
})
export class BlogGroupComponent implements OnInit {
  @Input() data: BlogGroup;
  @Input() blogGroupUrl: string;
  @Input() isAdmin: boolean;

  @Output() removeClicked: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onRemoveClicked = () => {
    this.removeClicked.emit(this.data);
  }
}
