import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import {
  BsDatepickerModule,
  ButtonsModule,
  ModalModule,
  PaginationModule,
  TabsModule,
  TimepickerModule
} from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { BlogPostComponent } from './pages/admin/blog-post/blog-post.component';
import { HomeComponent } from './pages/home/home.component';
import { BlogListComponent } from './pages/blog/list/list.component';
import { BlogDetailComponent } from './pages/blog/detail/detail.component';

import { acAppRoutes } from './app.routes';
import { AboutComponent } from './pages/about/about.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { AppInputComponent } from './components/app-input/app-input.component';
import { HttpRequestInterceptorService } from './services/http-request-interceptor.service';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { AuthService } from './services/auth.service';
import { SessionService } from './services/session.service';
import { NgxWigModule } from 'ngx-wig';
import { BlogPostService } from './services/blog-post.service';
import { BlogDetailListService } from './services/blog-detail-list.service';
import { AppBlogListComponent } from './components/app-blog-list/app-blog-list.component';
import { BlogPostGroupComponent } from './pages/admin/blog-post-group/blog-post-group.component';
import { BlogPostGroupService } from './services/blog-post-group.service';
import { BlogGroupComponent } from './components/blog-group/blog-group.component';
import { BlogGroupPageComponent } from './pages/blog/group/group.component';
import { BlogGroupListService } from './services/blog-group-list.service';
import { BlogGroupListPageComponent } from './pages/blog/group-list/group-list.component';
import { DateService } from './services/date.service';
import { AccountProfileComponent } from './pages/account/profile/profile.component';
import { UserService } from './services/user.service';
import { NgxStripeModule } from 'ngx-stripe';
import { CartComponent } from './pages/account/cart/cart.component';
import { CardTokenService } from './services/card-token.service';
import { BeehiveWysiwygModule } from 'beehive-wysiwyg';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    BlogPostComponent,
    HomeComponent,
    BlogListComponent,
    BlogDetailComponent,
    AboutComponent,
    RegistrationComponent,
    AppInputComponent,
    AppBlogListComponent,
    BlogPostGroupComponent,
    BlogGroupComponent,
    BlogGroupPageComponent,
    BlogGroupListPageComponent,
    AccountProfileComponent,
    CartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    acAppRoutes,
    BsDatepickerModule.forRoot(),
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    TimepickerModule.forRoot(),
    NgxWigModule,
    BeehiveWysiwygModule,
    NgxStripeModule.forRoot('pk_test_Y6RWIU9gzNWZs1WenBcYxaSV')
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpResponseInterceptorService, multi: true },
    AuthService,
    SessionService,
    BlogPostService,
    BlogPostGroupService,
    BlogDetailListService,
    BlogGroupListService,
    DateService,
    UserService,
    CardTokenService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
