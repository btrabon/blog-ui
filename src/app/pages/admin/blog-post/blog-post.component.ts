import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormPageBaseComponent } from '../../form.page.base.component';
import { FormControl } from '@angular/forms';
import { GeneralValidators } from '../../../validators';
import { BlogDetail, BlogDetailListService } from '../../../services/blog-detail-list.service';
import { BlogGroup, BlogPostGroupService } from '../../../services/blog-post-group.service';

@Component({
  selector: 'app-blog-post',
  templateUrl: './blog-post.component.html',
  styleUrls: ['./blog-post.component.scss']
})
export class BlogPostComponent extends FormPageBaseComponent implements OnInit {
  private id: string;
  blogPostGroups: BlogGroup[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private blogDetailListService: BlogDetailListService,
    private blogPostGroupService: BlogPostGroupService) {
    super();
  }

  ngOnInit() {
    this.blogPostGroupService.get()
      .subscribe(groups => this.blogPostGroups = groups);

    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        // when the id is passed we are loading a previous post
        this.id = params['id'];
        this.blogDetailListService.getById(params['id'])
          .subscribe((value: BlogDetail) => {
            this.form.patchValue(this.convertToFormGroup(value));
          });
      }
    });

    this.setFormGroup({
      articleGroup: new FormControl(null, null),
      title: new FormControl(null, [GeneralValidators.required.validate]),
      content: new FormControl(null, [GeneralValidators.required.validate]),
      dateToPublish: new FormControl(null, [GeneralValidators.required.validate]),
      price: new FormControl(null, null)
    });
  }

  onDateValueChanged(data) {
    // console.log('date changed ', data);
  }

  onSubmit() {
    if (this.form.valid) {
      const data = this.convertToApiData(this.form);

      if (!this.id) {
        this.blogDetailListService.create(data)
          .subscribe((value: BlogDetail) => {
            // this.form.patchValue(this.convertToFormGroup(value));
            this.router.navigateByUrl('/admin/dashboard');
          });
      } else {
        this.blogDetailListService.update(this.id, data)
          .subscribe((value: BlogDetail) => {
            // this.form.patchValue(this.convertToFormGroup(value));
            this.router.navigateByUrl('/admin/dashboard');
          });
      }
    }
  }

  private convertToFormGroup(data: BlogDetail) {
    const articleGroupId = data.article_group ? data.article_group.article_group_id : 0;

    // this nasty hack fixes the need for a timezone and allows use to store only the date without time
    const formattedDate = data.date_to_publish + 'T00:00:00.000Z';
    const finalDate = new Date(formattedDate);

    const formData = {
      articleGroup: articleGroupId,
      title: data.title,
      content: data.content,
      dateToPublish: finalDate,
      price: data.price,
      comments_enabled: false
    };

    return formData;
  }

  private convertToApiData(form) {
    const data = {
      article_group_id: form.get('articleGroup').value,
      title: form.get('title').value,
      content: form.get('content').value,
      date_to_publish: form.get('dateToPublish').value,
      price: form.get('price').value,
      comments_enabled: false
    };

    return data;
  }
}
