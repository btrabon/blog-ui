import { Component, OnInit } from '@angular/core';
import { BlogDetail, BlogDetailListService } from '../../../services/blog-detail-list.service';
import { BlogGroup, BlogPostGroupService } from '../../../services/blog-post-group.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  private blogDetails: BlogDetail[];
  private blogPostGroups: BlogGroup[];

  get blogListData(): BlogDetail[] {
    return this.blogDetails;
  }

  get blogGroupData(): BlogGroup[] {
    return this.blogPostGroups;
  }

  constructor(private blogDetailListService: BlogDetailListService, private blogPostGroupService: BlogPostGroupService) { }

  ngOnInit() {
    this.blogDetailListService.get()
      .subscribe(value => this.blogDetails = value);

    this.blogPostGroupService.get()
      .subscribe(value => this.blogPostGroups = value);
  }

  removePost(post: BlogDetail) {
    this.blogDetailListService.delete(post.article_id.toString())
      .subscribe(() => {
        this.blogDetailListService.get()
          .subscribe(value => this.blogDetails = value);
      });
  }

  removeGroup(group: BlogGroup) {
    this.blogPostGroupService.delete(group.article_group_id.toString())
      .subscribe(() => {
        this.blogPostGroupService.get()
          .subscribe(value => this.blogPostGroups = value);
      });
  }
}
