import { Component, OnInit } from '@angular/core';
import { FormPageBaseComponent } from '../../form.page.base.component';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BlogGroup, BlogPostGroupService } from '../../../services/blog-post-group.service';
import { FormControl } from '@angular/forms';
import { GeneralValidators } from '../../../validators';

@Component({
  selector: 'app-blog-post-group',
  templateUrl: './blog-post-group.component.html',
  styleUrls: ['./blog-post-group.component.scss']
})
export class BlogPostGroupComponent extends FormPageBaseComponent implements OnInit {
  private id: string;

  constructor(private route: ActivatedRoute, private router: Router, private blogPostGroupService: BlogPostGroupService) {
    super();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        // when the id is passed we are loading a previous post
        this.id = params['id'];
        this.blogPostGroupService.getById(params['id'])
          .subscribe((value: BlogGroup) => {
            this.form.patchValue(this.convertToFormGroup(value));
          });
      }
    });

    this.setFormGroup({
      title: new FormControl(null, [GeneralValidators.required.validate]),
      showAllLinks: new FormControl(null, null)
    });
  }

  onSubmit() {
    if (this.form.valid) {
      const data = this.convertToApiData(this.form);
      if (this.id === null || this.id === undefined || this.id.trim().length === 0) {
        this.blogPostGroupService.create(data)
          .subscribe((value: BlogGroup) => {
            this.router.navigateByUrl('/admin/dashboard');
          });
      } else {
        console.log('post group ', data);
        this.blogPostGroupService.update(this.id, data)
          .subscribe((value: BlogGroup) => {
            this.router.navigateByUrl('/admin/dashboard');
          });
      }
    }
  }

  private convertToFormGroup(data: BlogGroup) {
    const formData = {
      title: data.title,
      showAllLinks: data.show_all_links
    };

    return formData;
  }

  private convertToApiData(form) {
    const data = {
      title: form.get('title').value,
      show_all_links: form.get('showAllLinks').value || false
    };

    return data;
  }
}
