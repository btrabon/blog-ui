import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogPostGroupComponent } from './blog-post-group.component';

describe('BlogPostGroupComponent', () => {
  let component: BlogPostGroupComponent;
  let fixture: ComponentFixture<BlogPostGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogPostGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogPostGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
