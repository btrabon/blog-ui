import { Component, OnInit, ViewChild } from '@angular/core';
import { SessionService } from '../../../services/session.service';
import { BlogDetail, BlogDetailListService } from '../../../services/blog-detail-list.service';
import { FormPageBaseComponent } from '../../form.page.base.component';
import { FormControl } from '@angular/forms';
import { GeneralValidators } from '../../../validators';
import { ElementOptions, ElementsOptions, StripeCardComponent, StripeService, TokenResult } from 'ngx-stripe';
import { UserService } from '../../../services/user.service';
import { UserResponse } from '../../../services/auth.service';
import { CardTokenService } from '../../../services/card-token.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent extends FormPageBaseComponent implements OnInit {
  @ViewChild(StripeCardComponent) card: StripeCardComponent;

  articles: BlogDetail[] = [];
  isPaymentSuccessful = false;
  showPaymentButton = true;
  private currentUser: UserResponse;

  cardOptions: ElementOptions = {
    style: {
      base: {
        iconColor: '#666EE8',
        color: '#31325F',
        lineHeight: '40px',
        fontWeight: 300,
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSize: '18px',
        '::placeholder': {
          color: '#CFD7E0'
        }
      }
    }
  };

  elementsOptions: ElementsOptions = {
    locale: 'en'
  };

  get articleTotalPrice() {
    let totalPrice = 0;

    this.articles.forEach((item: BlogDetail) => {
      totalPrice += item.price;
    });

    return totalPrice;
  }

  constructor(private sessionService: SessionService,
              private blogDetailListService: BlogDetailListService,
              private userService: UserService,
              private cardTokenService: CardTokenService,
              private stripeService: StripeService) {
    super();
  }

  ngOnInit() {
    this.blogDetailListService.getByIds(this.sessionService.payToViewArticleIds)
      .subscribe((response: BlogDetail[]) => this.articles = response);
    this.userService.get()
      .subscribe((user: UserResponse) => this.currentUser = user);

    this.setFormGroup({
      name: new FormControl(null, [GeneralValidators.required.validate])
    });
  }

  onRemoveClick(article: BlogDetail) {
    const articleIndex = this.articles.indexOf(article);
    this.articles.splice(articleIndex, 1);

    this.sessionService.removeArticleFromCart(article.article_id);
  }

  onPurchaseClick() {
    this.isPaymentSuccessful = false;
    this.showPaymentButton = false;
    const name = this.form.get('name').value;

    this.stripeService.createToken(this.card.getCard(), { name })
      .subscribe((result: TokenResult) => {
        const tokenId = result.token.id;
        const amount = this.articleTotalPrice;
        const articleIds = this.sessionService.payToViewArticleIds;

        const data = {
          tokenId,
          amount,
          articleIds
        };
        this.cardTokenService.chargeCard(data)
          .subscribe(
            (response) => {
              this.articles = [];
              this.sessionService.clearCart();
              this.showPaymentButton = true;
              this.isPaymentSuccessful = true;
            },
            (error) => console.error(error)
          );
      });
  }
}
