import { Component, OnInit } from '@angular/core';
import { FormPageBaseComponent } from '../../form.page.base.component';
import { FormControl } from '@angular/forms';
import { GeneralValidators, PasswordValidators } from '../../../validators';
import { UserService } from '../../../services/user.service';
import { UserResponse } from '../../../services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class AccountProfileComponent extends FormPageBaseComponent implements OnInit {
  private isSaveSuccessful = false;
  oldPasswordValidated?: boolean = null;

  constructor(private userService: UserService) {
    super();
  }

  ngOnInit() {
    this.userService.get()
      .subscribe((value: UserResponse) => {
        this.form.patchValue(this.convertToFormGroup(value));
      });

    this.setFormGroup({
      firstName: new FormControl(null, [GeneralValidators.required.validate]),
      lastName: new FormControl(null, [GeneralValidators.required.validate]),
      phoneNumber: new FormControl(null, [GeneralValidators.required.validate]),
      email: new FormControl(null, [GeneralValidators.required.validate]),
      username: new FormControl(null, [GeneralValidators.required.validate]),
      previousPassword: new FormControl(null, null),
      newPassword: new FormControl(null, [
        PasswordValidators.lowercase.validate,
        PasswordValidators.uppercase.validate,
        PasswordValidators.numerals.validate,
        PasswordValidators.special.validate,
        PasswordValidators.length.validate
      ]),
      verifyPassword: new FormControl(null, null)
    }, [PasswordValidators.match.validate]);
  }

  onSubmit() {
    if (this.form.valid) {
      const data = this.convertFormGroupToApi();
      console.log('onSubmit data ', data);
      this.userService.update(data)
        .subscribe(this.handleSuccess, this.handleError);
    }
  }

  onPasswordChanged(event) {
    this.oldPasswordValidated = null;
    const previousPassword = event.target.value;

    this.userService.checkPassword(previousPassword)
      .subscribe((response: any) => this.oldPasswordValidated = response.passwords_equal);
  }

  private handleSuccess(response: UserResponse) {
    this.isSaveSuccessful = true;
  }

  private handleError(error: HttpErrorResponse) {
    console.error('save error ', error.message);
  }

  private convertFormGroupToApi() {
    const data = {
      firstname: this.form.get('firstName').value,
      lastname: this.form.get('lastName').value,
      phonenumber: this.form.get('phoneNumber').value,
      email: this.form.get('email').value,
      username: this.form.get('username').value,
      password: this.form.get('newPassword').value
    };

    return data;
  }

  private convertToFormGroup(data: UserResponse) {
    const formData = {
      firstName: data.firstname,
      lastName: data.lastname,
      phoneNumber: data.phonenumber,
      email: data.email,
      username: data.username
    };

    return formData;
  }
}
