import { Component, OnInit } from '@angular/core';
import { BlogDetail, BlogDetailListService } from '../../../services/blog-detail-list.service';
import { SessionService } from '../../../services/session.service';
import {Router} from "@angular/router";

@Component({
  selector: 'blog-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class BlogListComponent implements OnInit {
  private data: BlogDetail[];

  get blogListData(): BlogDetail[] {
    return this.data;
  }

  constructor(private blogDetailListService: BlogDetailListService,
              private sessionService: SessionService,
              private router: Router) { }

  ngOnInit() {
    this.blogDetailListService.get()
      .subscribe(value => this.data = value);
  }

  onPayButtonClicked(article: BlogDetail) {
    this.sessionService.addArticleToCart(article.article_id);
    this.router.navigateByUrl('/account/cart');
  }

  isArticleDisabled(article: BlogDetail): boolean {
    if (article.price > 0) {
      return !this.sessionService.isLoggedIn || article.can_view === false; // and the user hasn't paid for the article
    } else {
      return false;
    }
  }

  isPayToViewEnabled(article: BlogDetail): boolean {
    if (this.sessionService.isLoggedIn) {
      // we also need to add a check to see if the user has paid for this article already
      return article.price > 0 && article.can_view !== true;
    } else {
      return false;
    }
  }
}
