import { Component, OnInit } from '@angular/core';
import { BlogGroupListService } from '../../../services/blog-group-list.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ArticleGroup, BlogDetail } from '../../../services/blog-detail-list.service';
import { DateService } from '../../../services/date.service';
import {SessionService} from '../../../services/session.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class BlogGroupPageComponent implements OnInit {
  private data: BlogDetail[];
  private articleGroup: ArticleGroup;

  get blogGroupListData(): BlogDetail[] {
    const currentDate = new Date();
    const filteredData: BlogDetail[] = [];

    if (this.data) {
      for (let i = 0, length = this.data.length; i < length; i++) {
        const dateToPublishDate = new Date(this.data[i].date_to_publish);
        const differenceInDays = this.dateService.getDifferenceInDays(dateToPublishDate, currentDate);

        if (differenceInDays > 0) {
          if (this.articleGroup.show_all_links) {
            this.data[i].disabled = true;
            filteredData.push(this.data[i]);
          }
        } else {
          this.data[i].disabled = false;
          filteredData.push(this.data[i]);
        }
      }
    }

    return filteredData;
  }

  get blogGroupData(): ArticleGroup {
    return this.articleGroup;
  }

  constructor(private route: ActivatedRoute, private blogGroupListService: BlogGroupListService,
              private dateService: DateService, private sessionService: SessionService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        // when the id is passed we are loading a previous post
        this.blogGroupListService.get(params['id'])
          .subscribe(value => {
            this.data = value;
            this.articleGroup = value[0].article_group;
          });
      }
    });
  }

  onPayButtonClicked(article: BlogDetail) {
    this.sessionService.addArticleToCart(article.article_id);
  }

  isArticleDisabled(article: BlogDetail): boolean {
    if (article.price > 0) {
      return !this.sessionService.isLoggedIn || article.can_view === false; // and the user hasn't paid for the article
    } else {
      return article.disabled;
    }
  }

  isPayToViewEnabled(article: BlogDetail): boolean {
    if (this.sessionService.isLoggedIn) {
      // we also need to add a check to see if the user has paid for this article already
      return article.price > 0 && article.can_view !== true;
    } else {
      return false;
    }
  }
}
