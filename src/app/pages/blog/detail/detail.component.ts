import { Component, OnInit, ViewChild } from '@angular/core';
import { BlogDetail, BlogDetailListService } from '../../../services/blog-detail-list.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { BsModalRef, ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'blog-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class BlogDetailComponent implements OnInit {
  @ViewChild('autoShownModal') autoShownModal: ModalDirective;
  private data: BlogDetail;
  modalReference: BsModalRef;

  hasError: boolean;
  errorMessage: string;

  get blogData(): BlogDetail {
    return this.data;
  }

  constructor(private route: ActivatedRoute,
              private router: Router,
              private blogDetailListService: BlogDetailListService) { }

  ngOnInit() {
    this.hasError = false;
    this.route.params.subscribe((params: Params) => {
      if (params['id']) {
        const id: number = parseInt(params['id']);
        // when the id is passed we are loading a previous post
        this.blogDetailListService.getById(id)
          .subscribe(
            (value) => {
              this.data = value;
            },
            (error: HttpErrorResponse) => {
              this.hasError = true;
              if (error.status === 403) {
                this.errorMessage = 'You are not allowed to view this article';
              } else {
                this.errorMessage = error.message;
              }
            }
          );
      } else {
        this.blogDetailListService.getHomePage()
          .subscribe(value => this.data = value);
      }
    });
  }

  hideModal() {
    this.router.navigateByUrl('/');
  }
}
