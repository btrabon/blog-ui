import { Component, OnInit } from '@angular/core';
import { BlogGroup, BlogPostGroupService } from '../../../services/blog-post-group.service';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss']
})
export class BlogGroupListPageComponent implements OnInit {
  private data: BlogGroup[];

  get blogGroupListData() {
    return this.data;
  }

  constructor(private blogGroupListService: BlogPostGroupService) { }

  ngOnInit() {
    this.blogGroupListService.get()
      .subscribe((value: BlogGroup[]) => {
        this.data = value;
      });
  }

}
