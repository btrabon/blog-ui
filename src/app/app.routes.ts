import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/admin/dashboard/dashboard.component';
import { BlogPostComponent } from './pages/admin/blog-post/blog-post.component';
import { LoginComponent } from './pages/login/login.component';
import { BlogListComponent } from './pages/blog/list/list.component';
import { BlogDetailComponent } from './pages/blog/detail/detail.component';
import { AboutComponent } from './pages/about/about.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { AuthService } from './services/auth.service';
import { BlogPostGroupComponent } from './pages/admin/blog-post-group/blog-post-group.component';
import { BlogGroupPageComponent } from './pages/blog/group/group.component';
import { BlogGroupListPageComponent } from './pages/blog/group-list/group-list.component';
import { AccountProfileComponent } from './pages/account/profile/profile.component';
import { CartComponent } from './pages/account/cart/cart.component';

export const ROUTES: Routes = [
  {
    path: '',
    component: BlogDetailComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'blog',
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: BlogListComponent
      },
      {
        path: 'detail/:id',
        component: BlogDetailComponent
      },
      {
        path: 'group/:id',
        component: BlogGroupPageComponent
      },
      {
        path: 'group-list',
        component: BlogGroupListPageComponent
      }
    ]
  },
  {
    path: 'account',
    canActivate: [AuthService],
    children: [
      {
        path: 'profile',
        component: AccountProfileComponent
      },
      {
        path: 'cart',
        component: CartComponent
      }
    ]
  },
  {
    path: 'admin',
    canActivate: [AuthService],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'blog-post',
        component: BlogPostComponent
      },
      {
        path: 'blog-post/:id',
        component: BlogPostComponent
      },
      {
        path: 'blog-post-group',
        component: BlogPostGroupComponent
      },
      {
        path: 'blog-post-group/:id',
        component: BlogPostGroupComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'registration',
    component: RegistrationComponent
  }
];

export const acAppRoutes = RouterModule.forRoot(ROUTES, { enableTracing: false });
