import { Injectable } from '@angular/core';
import { ServiceBase } from './service.base';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators';
import { BlogDetail } from './blog-detail-list.service';
import { Observable, of } from 'rxjs';

@Injectable()
export class BlogGroupListService extends ServiceBase {
  private cachedData: BlogDetail[] = [];
  private cacheDataLoaded = false;

  constructor(private http: HttpClient) {
    super();
  }

  /**
   * Gets all articles that are part of a group.
   * @param {number} article_group_id - The article group to get articles from.
   * @param {boolean} cacheData - If true the retrieved data will be cached for retrieval later.
   * @returns {Array} The data retrieved from the API.
   */
  get(article_group_id: number = 0, cacheData: boolean = false) {
    if (!this.cacheDataLoaded && article_group_id > 0) {
      const url = this.createUrl('article_groups/' + article_group_id.toString() + '/articles');

      return this.http.get(url).pipe(
        map((response: BlogDetail[]) => {
          if (cacheData) {
            this.cachedData = response;
            this.cacheDataLoaded = true;
          }

          return response;
        })
      );
    } else {
      return of(this.cachedData).pipe(
        map((response: BlogDetail[]) => response)
      );
    }
  }

  /**
   * Used to retrieve the data cached during the call to the "get" method.
   * @returns {Array} The data cached during the call to the "get" method.
   */
  getCachedData() {
    return this.cachedData;
  }
}
