import { Injectable } from '@angular/core';

interface Session {
  token: string;
  isAdmin: boolean;
  tokenExpiry: Date;
  lastActivity: Date;
  roles: string[];
  payToViewArticleIds: number[];
}

@Injectable()
export class SessionService {
  private readonly _sessionKey = 'session';
  private sessionData: Session;

  constructor() {
    this.initSession();
  }

  initSession() {
    const sessionData = sessionStorage.getItem(this._sessionKey);
    if (sessionData === null || sessionData === '') {
      const session: Session = {
        token: null,
        isAdmin: false,
        tokenExpiry: new Date(Date.now()),
        lastActivity: new Date(Date.now()),
        roles: [],
        payToViewArticleIds: []
      };

      this.setSession(session, true);
    }
  }

  resetSession() {
    const session: Session = {
      token: null,
      isAdmin: false,
      tokenExpiry: new Date(Date.now()),
      lastActivity: new Date(Date.now()),
      roles: [],
      payToViewArticleIds: []
    };

    this.setSession(session, true);
  }

  get token(): string {
    return this.getSession().token;
  }

  set token(value: string) {
    const session = this.getSession();
    session.token = value;
    this.setSession(session);
  }

  get isLoggedIn(): boolean {
    const token = this.token;
    return token !== null && token !== '';
  }

  get isAdmin(): boolean {
    return this.getSession().isAdmin;
  }

  set isAdmin(value: boolean) {
    const session = this.getSession();
    session.isAdmin = value;
    this.setSession(session);
  }

  get tokenExpiry(): Date {
    return this.getSession().tokenExpiry;
  }

  set tokenExpiry(value: Date) {
    const session = this.getSession();
    session.tokenExpiry = value;
    this.setSession(session);
  }

  get lastActivity(): Date {
    return this.getSession().lastActivity;
  }

  set lastActivity(value: Date) {
    const session = this.getSession();
    session.lastActivity = value;
    this.setSession(session);
  }

  get roles(): string[] {
    return this.getSession().roles;
  }

  set roles(value: string[]) {
    const session = this.getSession();
    session.roles = value;
    this.setSession(session);
  }

  get payToViewArticleIds(): number[] {
    return this.getSession().payToViewArticleIds;
  }

  public addArticleToCart(articleId: number) {
    const session = this.getSession();
    const index = session.payToViewArticleIds.indexOf(articleId);
    if (index === -1) {
      session.payToViewArticleIds.push(articleId);
      this.setSession(session);
    }
  }

  public removeArticleFromCart(articleId: number) {
    const session = this.getSession();
    const index = session.payToViewArticleIds.indexOf(articleId);
    session.payToViewArticleIds.splice(index, 1);
    this.setSession(session);
  }

  public clearCart() {
    const session = this.getSession();
    session.payToViewArticleIds = [];
    this.setSession(session);
  }

  private getSession(): Session {
    if (!this.sessionData) {
      this.sessionData = JSON.parse(sessionStorage.getItem(this._sessionKey));
    }
    return this.sessionData as Session;
  }

  private setSession(session: Session, reloadSession: boolean = false) {
    sessionStorage.setItem(this._sessionKey, JSON.stringify(session));
    // this will be set to true when the session is initialized
    if (reloadSession) {
      this.sessionData = session;
    }
  }
}
