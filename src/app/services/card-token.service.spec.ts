import { TestBed, inject } from '@angular/core/testing';

import { CardTokenService } from './card-token.service';

describe('CardTokenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CardTokenService]
    });
  });

  it('should be created', inject([CardTokenService], (service: CardTokenService) => {
    expect(service).toBeTruthy();
  }));
});
