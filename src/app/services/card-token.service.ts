import { Injectable } from '@angular/core';
import { ServiceBase } from './service.base';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

export interface TokenCharge {
  tokenId: string;
  amount: number;
  articleIds: number[];
}

@Injectable()
export class CardTokenService extends ServiceBase {

  constructor(private http: HttpClient) {
    super();
  }

  chargeCard(token: TokenCharge) {
    const url = this.createUrl('card_token');

    return this.http.post(url, token).pipe(
      map(response => response)
    );
  }
}
