import { TestBed, inject } from '@angular/core/testing';

import { BlogPostGroupService } from './blog-post-group.service';

describe('BlogPostGroupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogPostGroupService]
    });
  });

  it('should be created', inject([BlogPostGroupService], (service: BlogPostGroupService) => {
    expect(service).toBeTruthy();
  }));
});
