import { Injectable } from '@angular/core';
import { ServiceBase } from './service.base';
import { HttpClient } from '@angular/common/http';
import {map} from "rxjs/internal/operators";
import {BlogDetail} from "./blog-detail-list.service";

export interface BlogGroup {
  article_group_id?: number;
  title: string;
  show_all_links: boolean;
}

@Injectable()
export class BlogPostGroupService extends ServiceBase {

  constructor(private http: HttpClient) {
    super();
  }

  get() {
    const url = this.createUrl('article_groups');

    return this.http.get(url).pipe(
      map((response: BlogGroup[]) => response)
    );
  }

  getById(id: string) {
    const url = this.createUrl('article_groups/' + id);

    return this.http.get(url).pipe(
      map((response: BlogGroup) => response)
    );
  }

  create(blogGroup) {
    const url = this.createUrl('article_groups');

    return this.http.post(url, blogGroup).pipe(
      map((response: BlogGroup) => response)
    );
  }

  update(id: string, blogGroup) {
    const url = this.createUrl('article_groups/' + id);

    return this.http.put(url, blogGroup).pipe(
      map((response: BlogGroup) => response)
    );
  }

  delete(id: string) {
    const url = this.createUrl('article_groups/' + id);

    return this.http.delete(url).pipe(
      map(response => response)
    );
  }
}
