import { Injectable } from '@angular/core';
import { ServiceBase } from './service.base';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UserRequest, UserResponse } from './auth.service';

@Injectable()
export class UserService extends ServiceBase {

  constructor(private http: HttpClient) {
    super();
  }

  /**
   * The get method will get the user that is currently signed in using the token.
   */
  get() {
    const url = this.createUrl('users');

    return this.http.get(url).pipe(
      map((response: UserResponse) => response)
    );
  }

  update(user: UserRequest) {
    const url = this.createUrl('users');

    return this.http.put(url, user).pipe(
      map((response: UserResponse) => response)
    );
  }

  delete() {
    const url = this.createUrl('users');

    return this.http.delete(url).pipe(
      map((response: UserResponse) => response)
    );
  }

  checkPassword(password: string) {
    const url = this.createUrl('auth/password');
    const data = {
      'password': password
    };

    return this.http.post(url, data).pipe(
      map(response => response)
    );
  }
}
