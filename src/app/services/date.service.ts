import { Injectable } from '@angular/core';

@Injectable()
export class DateService {

  constructor() { }

  /**
   * Gets the difference in two dates and returns the number of days.
   * @param {Date} beginDate - The date to subtract from the other.
   * @param {Date} endDate - The date to be subtracted from.
   * @returns {number} The number of days different between the two dates.
   */
  public getDifferenceInDays(beginDate: Date, endDate: Date) {
    const date1 = Date.UTC(beginDate.getFullYear(), beginDate.getMonth(), beginDate.getDate(),
      beginDate.getHours(), beginDate.getMinutes(), beginDate.getSeconds(), beginDate.getMilliseconds());
    const date2 = Date.UTC(endDate.getFullYear(), endDate.getMonth(), endDate.getDate(),
      endDate.getHours(), endDate.getMinutes(), endDate.getSeconds(), endDate.getMilliseconds());

    return Math.floor((date1 - date2) / 86400000);
  }
}
