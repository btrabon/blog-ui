import { Injectable } from '@angular/core';
import { ServiceBase } from './service.base';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators';

export interface ArticleGroup {
  article_group_id: number;
  title: string;
  show_all_links: boolean;
}

export interface OutputUser {
  app_user_id: number;
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  phonenumber: string;
  is_restricted: boolean;
}

export interface BlogDetail {
  article_id?: number;
  app_user_id: number;
  article_group_id?: number;
  title: string;
  content: string;
  price?: number;
  can_view?: boolean;
  comments_enabled?: boolean;
  date_to_publish: Date;
  promoted_start_date?: Date;
  promoted_end_date?: Date;
  created_date: Date;
  updated_date: Date;
  app_user?: OutputUser;
  article_group?: ArticleGroup;
  disabled?: boolean;
}

@Injectable()
export class BlogDetailListService extends ServiceBase {

  constructor(private http: HttpClient) {
    super();
  }

  get() {
    const url = this.createUrl('articles');

    return this.http.get(url).pipe(
      map((response: BlogDetail[]) => response)
    );
  }

  getById(id: number) {
    const url = this.createUrl('articles/' + id.toString());

    return this.http.get(url).pipe(
      map((response: BlogDetail) => response)
    );
  }

  getByIds(ids: number[]) {
    const url = this.createUrl('articles?article_ids=' + ids.join(','));

    return this.http.get(url).pipe(
      map((response: BlogDetail[]) => response)
    );
  }

  getHomePage() {
    const url = this.createUrl('articles?is_home_page=true');

    return this.http.get(url).pipe(
      map((response: BlogDetail) => response)
    );
  }

  create(blogPost) {
    const url = this.createUrl('articles');

    if (blogPost.article_group_id === 0) {
      blogPost.article_group_id = null;
    }

    return this.http.post(url, blogPost).pipe(
      map((response: BlogDetail) => response)
    );
  }

  update(id: string, blogPost) {
    const url = this.createUrl('articles/' + id);

    if (blogPost.article_group_id === 0) {
      blogPost.article_group_id = null;
    }

    return this.http.put(url, blogPost).pipe(
      map((response: BlogDetail) => response)
    );
  }

  delete(id: string) {
    const url = this.createUrl('articles/' + id);

    return this.http.delete(url).pipe(
      map(response => response)
    );
  }
}
