import { TestBed, inject } from '@angular/core/testing';

import { BlogGroupListService } from './blog-group-list.service';

describe('BlogGroupListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogGroupListService]
    });
  });

  it('should be created', inject([BlogGroupListService], (service: BlogGroupListService) => {
    expect(service).toBeTruthy();
  }));
});
