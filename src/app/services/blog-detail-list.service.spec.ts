import { TestBed, inject } from '@angular/core/testing';

import { BlogDetailListService } from './blog-detail-list.service';

describe('BlogDetailListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlogDetailListService]
    });
  });

  it('should be created', inject([BlogDetailListService], (service: BlogDetailListService) => {
    expect(service).toBeTruthy();
  }));
});
