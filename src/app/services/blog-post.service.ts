import { Injectable } from '@angular/core';
import { ServiceBase } from './service.base';
import { HttpClient } from '@angular/common/http';

export interface BlogPost {
  app_user_id: number;
  article_group_id: number;
  title: string;
  content: string;
  date_to_publish: Date;
  price: number;
}

export interface BlogPostResponse extends BlogPost {
  article_id: number;
}

@Injectable()
export class BlogPostService extends ServiceBase {

  constructor(private http: HttpClient) {
    super();
  }

  get(id?: number): BlogPostResponse {
    return null;
  }

  post(data: BlogPost): BlogPostResponse {
    return null;
  }
}
