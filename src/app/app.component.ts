import { Component } from '@angular/core';
import { setTheme } from 'ngx-bootstrap/utils';
import { Router } from '@angular/router';
import { SessionService } from './services/session.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private router: Router, private sessionService: SessionService) {
    setTheme('bs4');
  }

  get isAdminSite(): boolean {
    return this.router.url.indexOf('/admin/') > -1;
  }

  get isLoggedIn(): boolean {
    return this.sessionService.isLoggedIn;
  }

  get itemsInCart(): number {
    return this.sessionService.payToViewArticleIds.length;
  }

  logout() {
    this.sessionService.resetSession();
    this.router.navigateByUrl('/');
  }
}
